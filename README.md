# Rails 7 turbo with tinymce-rails. Case study
original application git@github.com:sergey-arkhipov/rails_tinyMCE.git

This is working example Rails 7 with turbo and tinymce-rails

## Gems versions

* ruby 3.1.2
* rails (7.0.4)
* tinymce-rails (6.3.1)
* tinymce-rails-langs (6.20220429)
* turbo-rails (1.3.2)
