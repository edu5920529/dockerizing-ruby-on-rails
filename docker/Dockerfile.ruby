FROM ruby:3.1.2 AS development

# Install Nodejs
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg -o /root/yarn-pubkey.gpg && apt-key add /root/yarn-pubkey.gpg
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y --no-install-recommends nodejs yarn

RUN mkdir -p /app

WORKDIR /app

COPY app/ .
COPY docker-entrypoint.sh .

RUN gem install bundler -v 2.3.17
RUN bundle install
RUN yarn install --check-files
# Start server
#CMD bundle exec rspec -f d
